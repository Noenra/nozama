<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220427111941 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE produits (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(45) NOT NULL, description LONGTEXT NOT NULL, image_link VARCHAR(255) NOT NULL, price INT NOT NULL, stock INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tags (id INT AUTO_INCREMENT NOT NULL, nametag VARCHAR(45) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tags_produits (tags_id INT NOT NULL, produits_id INT NOT NULL, INDEX IDX_D4BB43958D7B4FB4 (tags_id), INDEX IDX_D4BB4395CD11A2CF (produits_id), PRIMARY KEY(tags_id, produits_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE users (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, firstname VARCHAR(45) NOT NULL, lastname VARCHAR(45) NOT NULL, adress VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_1483A5E9E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE users_produits (users_id INT NOT NULL, produits_id INT NOT NULL, INDEX IDX_C56CF45667B3B43D (users_id), INDEX IDX_C56CF456CD11A2CF (produits_id), PRIMARY KEY(users_id, produits_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE messenger_messages (id BIGINT AUTO_INCREMENT NOT NULL, body LONGTEXT NOT NULL, headers LONGTEXT NOT NULL, queue_name VARCHAR(190) NOT NULL, created_at DATETIME NOT NULL, available_at DATETIME NOT NULL, delivered_at DATETIME DEFAULT NULL, INDEX IDX_75EA56E0FB7336F0 (queue_name), INDEX IDX_75EA56E0E3BD61CE (available_at), INDEX IDX_75EA56E016BA31DB (delivered_at), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE tags_produits ADD CONSTRAINT FK_D4BB43958D7B4FB4 FOREIGN KEY (tags_id) REFERENCES tags (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tags_produits ADD CONSTRAINT FK_D4BB4395CD11A2CF FOREIGN KEY (produits_id) REFERENCES produits (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE users_produits ADD CONSTRAINT FK_C56CF45667B3B43D FOREIGN KEY (users_id) REFERENCES users (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE users_produits ADD CONSTRAINT FK_C56CF456CD11A2CF FOREIGN KEY (produits_id) REFERENCES produits (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE tags_produits DROP FOREIGN KEY FK_D4BB4395CD11A2CF');
        $this->addSql('ALTER TABLE users_produits DROP FOREIGN KEY FK_C56CF456CD11A2CF');
        $this->addSql('ALTER TABLE tags_produits DROP FOREIGN KEY FK_D4BB43958D7B4FB4');
        $this->addSql('ALTER TABLE users_produits DROP FOREIGN KEY FK_C56CF45667B3B43D');
        $this->addSql('DROP TABLE produits');
        $this->addSql('DROP TABLE tags');
        $this->addSql('DROP TABLE tags_produits');
        $this->addSql('DROP TABLE users');
        $this->addSql('DROP TABLE users_produits');
        $this->addSql('DROP TABLE messenger_messages');
    }
}
