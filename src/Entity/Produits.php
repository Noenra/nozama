<?php

namespace App\Entity;

use App\Repository\ProduitsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ProduitsRepository::class)]
class Produits
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 45)]
    private $title;

    #[ORM\Column(type: 'text')]
    private $description;

    #[ORM\Column(type: 'string', length: 255)]
    private $image_link;

    #[ORM\Column(type: 'integer')]
    private $price;

    #[ORM\Column(type: 'integer')]
    private $stock;

    #[ORM\ManyToMany(targetEntity: Users::class, mappedBy: 'Produits_id')]
    private $Users_id;

    #[ORM\ManyToMany(targetEntity: Tags::class, mappedBy: 'Produits_id')]
    private $Tags_id;

    public function __construct()
    {
        $this->Users_id = new ArrayCollection();
        $this->Tags_id = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getImageLink(): ?string
    {
        return $this->image_link;
    }

    public function setImageLink(string $image_link): self
    {
        $this->image_link = $image_link;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getStock(): ?int
    {
        return $this->stock;
    }

    public function setStock(int $stock): self
    {
        $this->stock = $stock;

        return $this;
    }

    /**
     * @return Collection<int, Users>
     */
    public function getUsersId(): Collection
    {
        return $this->Users_id;
    }

    public function addUsersId(Users $usersId): self
    {
        if (!$this->Users_id->contains($usersId)) {
            $this->Users_id[] = $usersId;
            $usersId->addProduitsId($this);
        }

        return $this;
    }

    public function removeUsersId(Users $usersId): self
    {
        if ($this->Users_id->removeElement($usersId)) {
            $usersId->removeProduitsId($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, Tags>
     */
    public function getTagsId(): Collection
    {
        return $this->Tags_id;
    }

    public function addTagsId(Tags $tagsId): self
    {
        if (!$this->Tags_id->contains($tagsId)) {
            $this->Tags_id[] = $tagsId;
            $tagsId->addProduitsId($this);
        }

        return $this;
    }

    public function removeTagsId(Tags $tagsId): self
    {
        if ($this->Tags_id->removeElement($tagsId)) {
            $tagsId->removeProduitsId($this);
        }

        return $this;
    }
}
