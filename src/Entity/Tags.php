<?php

namespace App\Entity;

use App\Repository\TagsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TagsRepository::class)]
class Tags
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 45)]
    private $nametag;

    #[ORM\ManyToMany(targetEntity: Produits::class, inversedBy: 'Tags_id')]
    private $Produits_id;

    public function __construct()
    {
        $this->Produits_id = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNametag(): ?string
    {
        return $this->nametag;
    }

    public function setNametag(string $nametag): self
    {
        $this->nametag = $nametag;

        return $this;
    }

    /**
     * @return Collection<int, Produits>
     */
    public function getProduitsId(): Collection
    {
        return $this->Produits_id;
    }

    public function addProduitsId(Produits $produitsId): self
    {
        if (!$this->Produits_id->contains($produitsId)) {
            $this->Produits_id[] = $produitsId;
        }

        return $this;
    }

    public function removeProduitsId(Produits $produitsId): self
    {
        $this->Produits_id->removeElement($produitsId);

        return $this;
    }
}
